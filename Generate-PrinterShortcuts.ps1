<#---------------------------------------------------------------------------->
    .SYNOPSIS
        Generates shortcuts (*.url files) to printers on a Windows server.
    .DESCRIPTION
        Generates one shortcut file, each pointing to a printer on a Windows 
        server. Shortcut files are saved in a sub-folder on $PSScriptRoot.
        Expects argument containing the name of a Windows server. 
    .NOTES
        File Name      : Generate-PrinterShortcuts.ps1
        Author         : Jeremy Wong (wongj@mhusd.org)
        Prerequisite   : PowerShell and domain admin privileges
    .LINK
        https://bitbucket.org/mhusd/generate-printershortcuts/src/master/
    .EXAMPLE
        .\Generate-PrinterShortcuts.ps1 -server so-dc01
<-----------------------------------------------------------------------------#>

<#--- P A R A M E T E R S ----------------------------------------------------#>
param (
    [Parameter(Mandatory=$True)]
    [ValidateScript({Test-Connection $_ -quiet})]
    [string[]]$server
    # [string]$server = $(throw "Usage: .\Generate-PrinterShortcuts.ps1 -server `"<server-name>`"
    # Example: .\Generate-PrinterShortcuts.ps1 -server `"SO-DC01`"
    # ")
)
<#----------------------------------------------------------------------------#>

<#--- P A T H S --------------------------------------------------------------#>
$domain = $server
$outPath = "$PSScriptRoot\$domain"
<#----------------------------------------------------------------------------#>

<#--- O U T P U T ------------------------------------------------------------#>
$printers = Get-WmiObject -class Win32_printer -Computer $domain | Select-Object Name,Sharename

if(-not(Test-Path $outPath) -and ($printers.count -gt 0)) {
    New-Item -Path $outPath -type directory
}

$Shell = New-Object -ComObject ("WScript.Shell")

foreach($printer in $printers) {
    if( ($null -ne $printer.name) -and ($null -ne $printer.Sharename) ) {
        $outFile = "$outPath\$($printer.Name).url"
        $Shortcut = $Shell.CreateShortcut($outFile)
        $Shortcut.TargetPath = "file://$domain/$([uri]::EscapeDataString($printer.Sharename))"
        $Shortcut.Save()
    }
}