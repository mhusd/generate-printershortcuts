$path = $PSScriptRoot + "\Generate-PrinterShortcuts.ps1"

$domainControllers = Get-ADDomainController -Filter * -Server $env:USERDOMAIN | Select-Object hostname

foreach($domainController in $domainControllers) {
    Invoke-Expression -Command "$path -server $($domainController.hostname)"
}

